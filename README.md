ogsm交流群号:607328652

# osgm
osgm是一个基于web的osgi模块化开发技术。

# osgm 架构分层
分层架构如下：
osgm-platform 主平台层

osgm-commonbundle-基础层

osgm-utils  工具数据层

osgm-common 公共平台层

插件层待开发等：
 osgm-其他

# osgm 技术使用

使用apache Fliex作为启动

数据库使用mysql,暂不会考虑其他数据库，如有需求自己集成。


# osgm 使用

该系统只是一个集成框架，提供一系列开发解决方案。具体之后给出。

采用fliex自带的插件集成启动。启动效果如下：
![输入图片说明](https://gitee.com/uploads/images/2017/1219/114959_cf0b9dd2_626204.png "osgi.png")

启动过程等，可以查看文档下的开发部署文档
